import sys
sys.path.append('gen-py')

import subprocess
import threading
import re
import signal

from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.server import TServer

from omnibus import GameServer
from omnibus.GameServer import Iface
from omnibus.ttypes import GameServerStatus

from queue import Queue, Empty


class StarboundGame(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.users = set()
        self._stop_event = threading.Event()
             
    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()

    def run(self):
        process = subprocess.Popen(['./starbound_server'],
            bufsize=1,
            stdout=subprocess.PIPE,
            cwd='/home/steam/.steam/SteamApps/common/Starbound/linux')
        q = Queue()
        t = threading.Thread(target=self._enqueueOutput, args=(process.stdout, q))
        t.daemon = True # thread dies with the program
        t.start()

        while True:
            try:
                line = q.get(timeout=1)
            except Empty:
                pass
            else:
                line = line.decode('ascii').strip()
            
                # Do we have new output to process?
                if line:
                    print(line)
                    self._processLine(line)
                elif process.poll() is not None:
                    print('Game client has gone away')
                    break
            
            # We've been told to shutdown by the parent
            if self.stopped():
                print('Shutting down...')
                process.send_signal(signal.SIGINT)
                process.wait()
                break


    def _processLine(self, line):
        m = re.search(r"Client '(?P<name>\w+)' <\d+> \([a-z0-9:]+\) connected", line)
        if m is not None:
            groups = m.groupdict()
            self.users.add(groups['name'])
            return

        m = re.search(r"Client '(?P<name>\w+)' <\d+> \([a-z0-9:]+\) disconnected", line)
        if m is not None:
            groups = m.groupdict()
            self.users.discard(groups['name'])
            return
    
    def _enqueueOutput(self, out, queue):
        for line in iter(out.readline, b''):
            queue.put(line)
        out.close()


class Starbound(Iface):
    def __init__(self):
        self.game = None

    def ping(self):
        print('pong!')

    def status(self):
        return GameServerStatus.ONLINE if self._gameIsAlive() else GameServerStatus.OFFLINE
    
    def numPlayers(self):
        if not self._gameIsAlive():
            return 0
        return len(self.game.users)
    
    def playerList(self):
        if not self._gameIsAlive():
            return []
        return self.game.users

    def start(self):
        if not self._gameIsAlive():
            game = StarboundGame()
            game.start()
            self.game = game

    def stop(self):
        if self._gameIsAlive():
            self.game.stop()
            self.game.join()
            self.game = None
        
    def _gameIsAlive(self):
        return self.game is not None and self.game.isAlive()


if __name__ == '__main__':
    handler = Starbound()
    processor = GameServer.Processor(handler)
    transport = TSocket.TServerSocket(port=9090)
    tfactory = TTransport.TBufferedTransportFactory()
    pfactory = TBinaryProtocol.TBinaryProtocolFactory()

    server = TServer.TSimpleServer(processor, transport, tfactory, pfactory)

    server.serve()