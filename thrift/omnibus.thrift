enum GameServerStatus {
    OFFLINE,
    ONLINE,
}

service GameServer {
    void ping();
    GameServerStatus status();
    i16 numPlayers();
    set<string> playerList();
    void start();
    void stop();
}